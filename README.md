
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Covid-19 Mortality Visualisations

<!-- badges: start -->

<!-- badges: end -->

In this website we provide an overview of Covid-19 mortality in the most
affected countries of European, as well as West and Central Africa. The
source code can be used as a template to adapt to your own needs.

MOOD has received funding from the EU Horizon 2020 Research and
Innovation programme under grant agreement No 874850.

Contributors: [N. Alexander (Ergo)](neil.alexander@zoo.ox.ac.uk), [E.
Arsevska (Cirad)](elena.arsevska@cirad.fr), [W. Van Bortel
(ITM)](wvanbortel@itg.be), [S. Falala (Inrae)](sylvain.falala@cirad.fr),
[E. Van Kleef (ITM)](evankleef@itg.be), [R. Lancelot
(Cirad)](mailto:renaud.lancelot@cirad.fr), [C. Marsboom
(Avia-GIS)](cmarsboom@avia-gis.com), [F. Muñoz
(Cirad)](facundo.munoz@cirad.fr), [W. Wint
(Ergo)](william.wint@gmail.com)

  - [European Union and neighbouring
    countries](https://umr-astre.pages.mia.inra.fr/covid19-mortality/covid19MortalityEurope.html)

  - [West and Central
    Africa](https://umr-astre.pages.mia.inra.fr/covid19-mortality/covid19MortalityWCAfrica.html)

See the [source
code](https://forgemia.inra.fr/umr-astre/covid19-mortality)
