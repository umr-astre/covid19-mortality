# Reproducible environment
# Source for the docker image registry.forgemia.inra.fr/umr-astre/covid19-mortality
# (c) 2020 Facundo Muñoz

# Usage:
# - Reproduce CI locally:
#   sudo docker pull registry.forgemia.inra.fr/umr-astre/covid19-mortality
#   sudo docker run -e PASSWORD=fmstudio -p 8787:8787 --rm -it -v $(pwd):/home/rstudio/covid19-mortality registry.forgemia.inra.fr/umr-astre/covid19-mortality  # Run from the project root dir.
#   [browse to localhost:8787] username: rstudio, password: fmstudio
#   drake::r_make()
#
# - Modify and update Docker image
#   sudo docker build -t registry.forgemia.inra.fr/umr-astre/covid19-mortality .
#   [test]
#   sudo docker push registry.forgemia.inra.fr/umr-astre/covid19-mortality


FROM rocker/geospatial

LABEL maintainer="Facundo Muñoz facundo.munoz@cirad.fr"

# Install external dependencies

## Install MS Fonts (using Arial for the pdf document)
## Pre-accept licence
## https://github.com/captnswing/msttcorefonts
RUN echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections
RUN apt-get install -y --no-install-recommends fontconfig ttf-mscorefonts-installer

RUN export DEBIAN_FRONTEND=noninteractive; apt-get -qq update \
&& apt-get install -y --no-install-recommends \
fontconfig \
ttf-mscorefonts-installer \
# libv8-dev \
# libjq-dev \
# libprotobuf-dev \
# protobuf-compiler \
# libfontconfig1-dev \
# gdal-bin \
# libgdal-dev \
# libglpk-dev \
# libgmp-dev \
# libproj-dev \
# libxml2-dev \
# libudunits2-dev \
# libcairo2-dev \
# qpdf \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/


# Install required LaTeX packages for pdf reports
RUN tlmgr update --self \
&& tlmgr install \
amsfonts \
amsmath \
latex-amsmath-dev \
iftex \
kvoptions \
ltxcmds \
kvsetkeys \
etoolbox \
atbegshi \
atveryend \
auxhook \
bigintcalc \
bitset \
etexcmds \
gettitlestring \
hycolor \
hyperref \
intcalc \
kvdefinekeys \
letltxmacro \
pdfescape \
refcount \
rerunfilecheck \
stringenc \
uniquecounter \
url \
zapfding \
pdftexcmds \
infwarerr \
geometry \
latex-tools-dev \
booktabs \
grffile \
epstopdf-pkg \
euenc \
fontspec \
lm-math \
tipa \
unicode-math \
xunicode

# Install R-package dependencies for compiling reports
RUN ["install2.r", \
"ade4", \
"Cairo", \
"countrycode", \
"cowplot", \
"distill", \
"downloader", \
"drake", \
"furrr", \
"future.callr", \
"gratia", \
"here", \
"hrbrthemes", \
"janitor", \
"kableExtra", \
"knitr", \
"latticeExtra", \
"mvnfast", \
"NCmisc", \
"pacman", \
"patchwork", \
"proftools", \
"reader", \
"readxl", \
"rjson", \
"rmarkdown", \
"rmdformats" \
]

## Only needed with the option fig_crop
## which gives issues with device Cairo
## https://github.com/yihui/knitr/issues/1365
# RUN ["install2.r", "magick"]

# Set up environment
RUN echo 'alias ll="ls -lh --color=tty"' >> ~/.bashrc
