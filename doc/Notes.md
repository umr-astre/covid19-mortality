---
title: Title project 
author: Facundo Muñoz
date: "`r format(Sys.Date(), '%b, %Y')`"
output:
  pdf_document: default
  html_document: default
documentclass: cirad
---



## Context

meeting date - people

## Data

TESSY data (hospital admissions). Files `NCOV_Aggregated.csv`, `NCOV_Case.csv`.

## Proposed Analyses



## Remarks

